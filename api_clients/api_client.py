import abc
import pandas as pd
import numpy as np


class BankClient(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def fetch_single_code(self, series_id, initial_date=None, end_date=None): 
        pass

    def fetch(self, series_id, initial_date=None, end_date=None):
        """
        Returns series from the SGS

        Args:
            series_id (int, str, list of int or list of str): series code on the SGS.
            initial_date (str): initial date for the result
            end_date (str): end date for the result

        Returns:
            pd.DataFrame: with the requested series.If a dict is passed as series ID, the dict values are used
                        as column names.
        """

        if isinstance(series_id, list):

            df = self._fech_from_list(series_id, initial_date, end_date).sort_index(inplace=True)
        
        elif isinstance(series_id, dict):

            codes = series_id.keys()

            df = self._fech_from_list(codes, initial_date, end_date)

            df.columns = series_id.values()
        
        elif isinstance(series_id, int) or isinstance(series_id, str):

            df = self.fetch_single_code(series_id, initial_date=initial_date, end_date=end_date)
        
        else:

            raise TypeError(f'expected either list, dict, int or str, found: {type(series_id)}')

        return df

    def _fech_from_list(self, series_id, initial_date, end_date):

        data_arr = np.empty(len(series_id), dtype=np.object)

        for idx, cod in enumerate(series_id):

            data_arr[idx] = self.fetch_single_code(cod, initial_date=initial_date, end_date=end_date)

        return pd.concat(data_arr, axis=1)


class BCBClient(BankClient):

    def __init__(self, base_url='http://api.bcb.gov.br/dados/serie/bcdata.sgs.'):

        self.base_url = base_url

    def fetch_single_code(self, series_id, initial_date=None, end_date=None):
        """
        Returns a single series using the API of the SGS. Queries are built using the url
        http://api.bcb.gov.br/dados/serie/bcdata.sgs.{seriesID}/dados?formato=json&dataInicial={initial_date}&dataFinal={end_date}

        The url returns a json file which is read and parsed to a pandas DataFrame.

        ISSUE: It seems like the SGS API URL is not working properly. Even if you pass and initial or end date argument,
        the API does not filter the dates and always returns all the available values. I added a date filter for the
        pandas dataframe that is passed to the user and left the old code intact, in case this gets corrected in the
        future.

        Args:
            series_id (pd.DataFrame):series code on the SGS
            initial_date (str): initial date for the result (optional)
            end_date (str): end date for the result (optional)

        Returns:
            pd.DataFrame: with a single series as column and the dates as the index
        """

        url = self._build_url(series_id, initial_date, end_date)

        df = pd.read_json(url)
        df = df.set_index(pd.to_datetime(df['data'], dayfirst=True)).drop('data', axis=1)
        df.columns = [str(series_id)]

        return self._correct_dates(df, initial_date, end_date)

    def _build_url(self, series_id, initial_date=None, end_date=None):
        """ returns the search url as string """

        url = f'{self.base_url}{series_id}/dados?formato=json'

        if initial_date:
            url += f'&dataInicial={initial_date}'
        if end_date:
            url += f'&dataFinal={end_date}'

        return url

    @staticmethod
    def _correct_dates(df, initial_date, end_date):
        if initial_date:
            dt_ini = pd.to_datetime(initial_date, dayfirst = True)
            df = df[df.index >= dt_ini]

        if end_date:
            dt_end = pd.to_datetime(end_date, dayfirst = True)
            df = df[df.index <= dt_end]

        return df
