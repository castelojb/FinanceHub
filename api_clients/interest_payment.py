from api_clients.api_client import BCBClient
import pandas as pd

CDI_COL = 'Taxa de juros - CDI'
SELIC_COL = 'Taxa de juros - SELIC'

CDI_CODE = 12
SELIC_CODE = 11

keys_map = {

    'selic': SELIC_CODE,
    'cdi': CDI_CODE
}


def make_interest_payment_bus_252(interest_rate_arr: float) -> float:
    """
    Returns the daily bus252

    Args:
        interest_rate_arr (float): the annual interest rate in decimal

    Returns:
        float: The daily interest rate
    """

    acum_bus = (((1 + interest_rate_arr)**(1/252)) - 1) * 100
    
    return acum_bus


def get_series(keys_set, initial_date=None, end_date=None, with_aditional_info=True) -> pd.DataFrame:
    """
    Return a time series DataFrame from the SGS.

    Args:
        keys_set (list or tuple): A collection of interest rate names
        initial_date (str): initial date for the result
        end_date (str): end date for the result
        with_aditional_info (bool): can add a col of interest payment rate

    Returns:
         pd.DataFrame: A DataFrame with time series index
    """

    dict_ = dict()

    generate_col = lambda key: f'Interest Rate - {key.upper()}'
    for key in keys_set:

        code = keys_map[key]

        dict_[code] = generate_col(key)
    
    client = BCBClient()

    df = client.fetch(dict_, initial_date=initial_date, end_date=end_date)

    if with_aditional_info:

        generate_interest_payment_col = lambda key: f'Interest Payment - {key.upper()}'
        
        for key in keys_set:

            col = generate_col(key)

            arr = df[col].to_numpy()

            interest_payment = make_interest_payment_bus_252(arr)

            interest_payment_col = generate_interest_payment_col(key)

            df[interest_payment_col] = interest_payment
    
    return df
